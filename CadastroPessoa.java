import java.io.*;

public class CadastroPessoa{



	public static void main(String[] args) throws IOException{
		//burocracia para leitura de teclado
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;

		//instanciando objetos do sistema
		ControlePessoa umControle = new ControlePessoa();

		//interagindo com usuário
		do{
			System.out.println("=================MENU=================");
			System.out.println("Escreva add para adicionar uma pessoa.");
			System.out.println("Escreva remove para remover uma pessoa.");
			System.out.println("Escreva search para buscar uma pessoa.");
			System.out.println("Escreva quit para encerrar o programa.");
			entradaTeclado = leitorEntrada.readLine();
			if(entradaTeclado.equalsIgnoreCase("add")){
			
				System.out.println("Digite o nome da Pessoa:");
				entradaTeclado = leitorEntrada.readLine();
				String umNome = entradaTeclado;
			
				System.out.println("Digite o telefone da Pessoa:");
				entradaTeclado = leitorEntrada.readLine();
				String umTelefone = entradaTeclado;
			
				Pessoa umaPessoa = new Pessoa(umNome, umTelefone);
				String mensagem = umControle.adicionar(umaPessoa);
				//conferindo saída
				System.out.println("======================================");
				System.out.println(mensagem);
				System.out.println("=)");
			}
			if(entradaTeclado.equalsIgnoreCase("remove")){
				System.out.println("Digite o nome da Pessoa a ser removida:");
                        	entradaTeclado = leitorEntrada.readLine();
                        	String umNome = entradaTeclado;
				
				Pessoa umaPessoa = new Pessoa(umNome);
				String mensagem = umControle.remover(umaPessoa);
				
				System.out.println("======================================");
                                System.out.println(mensagem);
                                System.out.println("=)");
			}
			if(entradaTeclado.equalsIgnoreCase("search")){
				System.out.println("Digite o nome da Pessoa a ser buscada:");
                                entradaTeclado = leitorEntrada.readLine();
                                String umNome = entradaTeclado;

                                Pessoa umaPessoa  = umControle.pesquisar(umNome);
				if(!umaPessoa.equals(null)){
					System.out.println("======================================");
                                	System.out.println("Pessoa encontrada!");
                                	System.out.println("=)");
				}else{
					System.out.println("======================================");
                                        System.out.println("Pessoa não encontrada...");
                                        System.out.println("=(");
				}
			}

		}while(!entradaTeclado.equalsIgnoreCase("quit"));
	}
}
